import Calendar from 'react-calendar'; 
import 'react-calendar/dist/Calendar.css';
export default function App() {
  
  return (
    <div className='w-full h-full p-10'>
      <Calendar className='w-96 h-full rounded-xl bg-violet-300'/>
    </div>
  
  )
}
